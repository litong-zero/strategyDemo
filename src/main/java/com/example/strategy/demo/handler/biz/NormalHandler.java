package com.example.strategy.demo.handler.biz;

/**
 * @author litong
 * @date 2019/12/20 11:12
 */
import com.example.strategy.demo.entity.OrderBO;
import com.example.strategy.demo.handler.AbstractHandler;
import com.example.strategy.demo.handler.HandlerType;
import org.springframework.stereotype.Component;


@Component
@HandlerType("1")
public class NormalHandler extends AbstractHandler {

    @Override
    public String handle(OrderBO bo) {
        return "处理普通订单";
    }

}
