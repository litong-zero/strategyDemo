package com.example.strategy.demo.handler.biz;

/**
 * @author litong
 * @date 2019/12/20 11:12
 */
import com.example.strategy.demo.entity.OrderBO;
import com.example.strategy.demo.handler.AbstractHandler;
import com.example.strategy.demo.handler.HandlerType;
import org.springframework.stereotype.Component;


@Component
@HandlerType("3")
public class PromotionHandler extends AbstractHandler {

    @Override
    public String handle(OrderBO bo) {
        return "处理促销订单";
    }

}

