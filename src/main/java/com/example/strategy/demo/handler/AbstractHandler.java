package com.example.strategy.demo.handler;

import com.example.strategy.demo.entity.OrderBO;

/**
 * @author litong
 * @date 2019/12/20 11:11
 */
public abstract class AbstractHandler {

    /**
     * s
     * @param bo
     * @return
     */
    abstract public String handle(OrderBO bo);

}
