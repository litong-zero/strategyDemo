package com.example.strategy.demo.handler;

/**
 * @author litong
 * @date 2019/12/20 11:08
 */
import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface HandlerType {

    String value();

}
