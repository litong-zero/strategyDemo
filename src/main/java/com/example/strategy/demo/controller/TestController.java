package com.example.strategy.demo.controller;

import com.example.strategy.demo.entity.OrderBO;
import com.example.strategy.demo.handler.AbstractHandler;
import com.example.strategy.demo.handler.HandlerContext;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author litong
 * @date 2019/12/20 13:31
 */
@RestController
@RequestMapping("/test")
@AllArgsConstructor
public class TestController {
    private HandlerContext handlerContext;

    @GetMapping("/{type}")
    public String test(@PathVariable String type) {
        AbstractHandler instance = handlerContext.getInstance(type);
        return instance.handle(new OrderBO());
    }

}
